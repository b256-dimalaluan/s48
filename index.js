// Dummy Database 

let posts = [];
let count = 1;

// Create/Add Posts
document.querySelector('#form-add-post').addEventListener('submit', (e) => {

	// this prevents the webpage from reloading 
	e.preventDefault();


	// actslike a schema/model of the document
	posts.push({
		id:count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});

	// this is responsible to increment the ID value 
	count++;


	console.log(posts);
	showPost(posts);
	alert('Successfullt Added!');
})

// Display/Show Posts
const showPost = (posts) => {

	let postEntries = '';

	posts.forEach((post) => {

		postEntries += `
			<div id='post-${post.id}'>
				<h3 id='post-title-${post.id}'>${post.title}</h3>
				<p id='post-body-${post.id}'>${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;

	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Edit Posts

const editPost = (postId) => {

	let title = document.querySelector(`#post-title-${postId}`).innerHTML;
	let body = document.querySelector(`#post-body-${postId}`).innerHTML;

	document.querySelector('#txt-edit-id').value = postId;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

}

// Update Posts 
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault();

	// To Find the specific post we want to edit
	for(let i=0; i<posts.length; i++) {

		// The values post[i].id is a NUMBER while document.querySelector("#txt-edit-id").value is a STRING
		// Therefore, it is necessary to convert the NUMBER to a STRING
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
			posts[i].title = document.querySelector('#txt-edit-title').value
			posts[i].body = document.querySelector('#txt-edit-body').value

			showPost(posts);
			alert("Successfully Updated na po ang edit!");

			break;
		}
	}
})

// Delete Posts(activity)

function deletePost(postId) {


    

    //finding the index of the post
    const postIndex = posts.findIndex(post => post.id === postId);

    // removing from array from the array
    if (postIndex !== -1) {
        posts.splice(postIndex, 1);
    }

    // Removing the post 
    const postDiv = document.querySelector(`#post-${postId}`);
    if (postDiv) {
        postDiv.remove();
    }
}

